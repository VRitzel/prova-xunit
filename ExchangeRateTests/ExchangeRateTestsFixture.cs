﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;
using Prova;

namespace Prova.Test
{
    public class ExchangeRateTestsFixture
    {
        public ExchangeRate GenerateValidExchangeRate() {
            return new(
                new DateOnly(2023, 10, 10),
                100.1234567891m,
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()
            );
        }

        public ExchangeRate GenerateInvalidFactor()
        {
            return new(
                new DateOnly(2023, 10, 10),
                -1,
                Guid.NewGuid(),
                Guid.NewGuid(),
                Guid.NewGuid()

            );
        }

        public ExchangeRate GenerateInvalidTypeId()
        {
            return new(
                new DateOnly(2023, 10, 10),
                100.1234567891m,
                Guid.Empty,
                Guid.NewGuid(),
                Guid.NewGuid()

            );
        }

    }
}
