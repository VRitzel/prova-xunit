using Xunit.Abstractions;
using Xunit.Sdk;

namespace Prova.Test
{
    public class ExchangeRateTests : IClassFixture<ExchangeRateTestsFixture>
    {
        private ExchangeRateTestsFixture _fixture;
        readonly ITestOutputHelper _outputHelper;

        public ExchangeRateTests(ExchangeRateTestsFixture fixture, ITestOutputHelper outputHelper)
        {
            _fixture = fixture;
            _outputHelper = outputHelper;
        }

        [Fact(DisplayName = "new Valid Exchange Rate")]
        [Trait("Category", "Exchange Rate Entity Unit Test")]
        public void IsValid_WhenExchangeRateIsValid_ShouldReturnTrueAndHaveNoErrors()
        {
            // arrange
            var exchangeRate = _fixture.GenerateValidExchangeRate();
            var factorRounded = exchangeRate.Factor.Scale.Equals(5);
            var testError = () => { throw new InvalidOperationException(); };
            var execution = Record.Exception(testError);    

            // assert
          //  Assert.NotNull(execution);
          //  Assert.IsType<InvalidOperationException>(execution);
            Assert.True(factorRounded);
            // Assert.Throws<DomainException>(_fixture.GenerateValidExchangeRate);
        }

        [Fact(DisplayName = "Factor is negative or null")]
        [Trait("Category", "Exchange Rate Entity Unit Test")]
        public void IsValid_WhenFactorIsNegative_ShouldReturnFalseAndHaveErrors()
        {
            // arrange
            var exchangeRate = _fixture.GenerateInvalidFactor();

            // assert
            Assert.Throws<DomainException>(_fixture.GenerateInvalidFactor);
        }

        [Fact(DisplayName = "TypeId is null")]
        [Trait("Category", "Exchange Rate Entity Unit Test")]
        public void IsValid_WhenTypeIsNull_ShouldReturnFalseAndHaveErrors()
        {
            var exchangeRate = _fixture.GenerateInvalidTypeId();

            Assert.Throws<DomainException>(_fixture.GenerateInvalidTypeId);
        }
    }
}