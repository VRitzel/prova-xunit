﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Prova
{
    public class ExchangeRate
    {

        public DateOnly QuotationDate { get; private set; }
        public decimal Factor { get; private set; }
        public Guid TypeId { get; private set; }
        public Guid CurrencyOriginId { get; private set; }
        public Guid CurrencyDestinationId { get; private set; }


        public ExchangeRate(
            DateOnly quotationDate,
            decimal factor,
            Guid typeId,
            Guid currencyOriginId,
            Guid currencyDestinationId)
        {
            QuotationDate = quotationDate;
            Factor = Math.Round(factor, 5);
            TypeId = typeId;
            CurrencyOriginId = currencyOriginId;
            CurrencyDestinationId = currencyDestinationId;

            Validate();
        }


        public void Validate()
        {
            AssertionConcern.AssertArgumentLessThan(
                QuotationDate,
                DateOnly.FromDateTime(DateTime.Now),
                "The QuotationDate cannot be in the future");

            AssertionConcern.AssertArgumentGreaterThan(
                Factor,
                0,
                "The Factor cannot be zero");

            AssertionConcern.AssertArgumentNotEmpty(
                TypeId,
                "The TypeId cannot be empty");

            AssertionConcern.AssertArgumentNotEmpty(
                CurrencyOriginId,
                "The CurrencyOriginId cannot be empty");

            AssertionConcern.AssertArgumentNotEmpty(
                CurrencyDestinationId,
                "The CurrencyDestinationId cannot be empty");
        }
    }
}

